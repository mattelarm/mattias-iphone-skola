//
//  Backend.m
//  ClassTest
//
//  Created by IT-Högskolan on 2015-02-01.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "Backend.h"

@implementation Backend


-(NSString*)createNewStory {
    self.num1 = @[@"hjälte", @"tjuv", @"programmerare", @"skojare", @"telefonförsäljare"];
    
    self.num2 = @[@"Mattias", @"Egon", @"Albin", @"Pontus", @"Johan"];
    
    self.num3 = @[@"Snickare", @"Bonde", @"Idiot", @"Tattare", @"Öldrickare"];
    
    self.num4 = @[@"ett hus", @"en lägenhet", @"en bil", @"en husvagn utan dörr", @"ett slott"];
    
    self.num5 = @[@"i stan", @"ute på landet", @"nära soptippen", @"vid vattnet"];
    
    self.num6 = @[@"spela fickpingis", @"fiska", @"dricka cola", @"virka", @"jaga rådjur"];
    
    self.num7 = @[@"kväll", @"morgon", @"dag", @"natt"];
    
    self.num8 = @[@"löptur", @"prommenad", @"joggingtur", @"simmtur"];
    
    self.num9 = @[@"Sofia", @"Stina", @"Mikaela", @"Therese", @"Johanna"];
    
    self.num10 = @[@"plocka blommor", @"kolla på film", @"shoppa kläder", @"leka krig", @"sova"];
    
    NSString* story = [self makeStory];
    return story;
}

-(NSString*)randomElement:(NSArray*)array {
    return array[arc4random() % array.count];
}


-(NSString*)makeStory {

    NSString* randomnum1 = [self randomElement:self.num1];
    NSString* randomnum2 = [self randomElement:self.num2];
    NSString* randomnum3 = [self randomElement:self.num3];
    NSString* randomnum4 = [self randomElement:self.num4];
    NSString* randomnum5 = [self randomElement:self.num5];
    NSString* randomnum6 = [self randomElement:self.num6];
    NSString* randomnum7 = [self randomElement:self.num7];
    NSString* randomnum8 = [self randomElement:self.num8];
    NSString* randomnum9 = [self randomElement:self.num9];
    NSString* randomnum10 = [self randomElement:self.num10];
    
 NSString* myString = [NSString stringWithFormat:@"Det var en gång en %@ som hette %@. Han var född till %@ men jobbade nu som %@ på heltid. %@ bodde i %@ %@. När han inte jobbade som %@ så fick han tiden att gå genom att  %@. Han var lite av ett proffs på sin hobby att %@, resten av killarna i staden var avundsjuka på honom. På helgerna brukade han åka ut till en sjö och fiska. Där kunde han få vara sig själv för en stund. En %@ när han var på sin vanliga %@ mötte han en prinsessa vid namn %@. Hon var väldigt söt och dom fick bra kontakt direkt. Efter lite drygt två veckor så frågade hon honom om han var bra på att %@. Ja svarade %@ det är det bästa jag vet! Då frågade hon honom om han ville följa med och %@. Det ville han och så levde dom lyckliga i alla sina dagar." ,randomnum1,randomnum2,randomnum3,randomnum1,randomnum2,randomnum4,randomnum5,randomnum3,randomnum6,randomnum6,randomnum7,randomnum8,randomnum9,randomnum6,randomnum2,randomnum10];
    
    return myString;
}


@end
