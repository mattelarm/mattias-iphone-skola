//
//  Backend.h
//  ClassTest
//
//  Created by IT-Högskolan on 2015-02-01.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Backend : NSObject
@property (nonatomic) NSArray *num1;
@property (nonatomic) NSArray *num2;
@property (nonatomic) NSArray *num3;
@property (nonatomic) NSArray *num4;
@property (nonatomic) NSArray *num5;
@property (nonatomic) NSArray *num6;
@property (nonatomic) NSArray *num7;
@property (nonatomic) NSArray *num8;
@property (nonatomic) NSArray *num9;
@property (nonatomic) NSArray *num10;


-(NSString*)createNewStory;


@end
