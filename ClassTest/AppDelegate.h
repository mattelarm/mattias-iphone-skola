//
//  AppDelegate.h
//  ClassTest
//
//  Created by IT-Högskolan on 2015-02-01.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

