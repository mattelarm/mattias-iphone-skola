//
//  ViewController.m
//  ClassTest
//
//  Created by IT-Högskolan on 2015-02-01.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "ViewController.h"
#import "Backend.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextView *storyText;

@end

@implementation ViewController


- (IBAction)createStoryButton:(id)sender {
    Backend *myBackend = [[Backend alloc]init];
    self.storyText.text = [myBackend createNewStory];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
